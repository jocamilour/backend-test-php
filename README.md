**Alcabama Backend Test – PHP**

Hola y muchas gracias por tomar el tiempo para realizar la prueba.

El objetivo de la prueba es desarrollar una aplicación PHP para gestionar las reservas de un restaurante, teniendo en cuenta:
- Uso de Laravel 5.
- GIT.
- Bootstrap.
- MySQL ó PostgreSQL.
- JavaScript.
- Ajax.
- En la reserva el usuario puede reservar más de una mesa.
- Se debe comprobar la disponibilidad.

**Se valorará:**
- CRUD completo de reservas y usuarios.
- La organización del código.
- Comprobación de errores.

**Entrega:**
Por favor cree un branch con su nombre y cuando termine envíe un correo a programacion@alcabama.com.co para iniciar el proceso de revisión.